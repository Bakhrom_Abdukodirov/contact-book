<?php

namespace Database\Seeders;

use App\Models\Contact;
use App\Models\User;
use App\Models\UserContact;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserContactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contacts = Contact::query()->get();
        foreach ($contacts as $contact){
            $data =[
                'user_id' => User::query()->inRandomOrder()->first()->id,
                'contact_id' => $contact->id,
            ];
            UserContact::query()->create($data);
        }
        DB::statement('ALTER TABLE user_contacts AUTO_INCREMENT ' . UserContact::query()->count());
    }
}
