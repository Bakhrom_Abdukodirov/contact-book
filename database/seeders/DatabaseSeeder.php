<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ContactTableSeeder::class);
        $this->call(UserContactTableSeeder::class);
        $this->call(ContactEmailTableSeeder::class);
        $this->call(ContactPhoneTableSeeder::class);
    }
}
