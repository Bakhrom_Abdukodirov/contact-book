<?php

namespace Database\Seeders;

use App\Models\ContactEmail;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContactEmailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ContactEmail::factory()
            ->count(20)
            ->create();
        DB::statement('ALTER TABLE contact_emails AUTO_INCREMENT ' . ContactEmail::query()->count());
    }
}
