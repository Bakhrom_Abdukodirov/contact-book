<?php

namespace Database\Seeders;

use App\Models\ContactPhone;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContactPhoneTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ContactPhone::factory()
            ->count(20)
            ->create();
        DB::statement('ALTER TABLE contact_phones AUTO_INCREMENT ' . ContactPhone::query()->count());
    }
}
