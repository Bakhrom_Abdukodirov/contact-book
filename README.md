<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

## About Project
# Requirements
1. PHP ^8.1
2. MySql 8.0
3. Composer 2.2

# Installation
1. Clone the repository
2. Run `composer install`
3. Run `cp .env.example .env`
4. Run `php artisan key:generate`
5. Run `php artisan jwt:secret`
6. Run `php artisan migrate`
7. Run `php artisan db:seed`
8. Run `php artisan serve`
9. Open [http://localhost:8000](http://localhost:800) in your browser


1. Run `php artisan queue:work` in other terminal 
2. Run `php artisan schedule:run` in other terminal
3. You should set smtp.mail.ru settings to env file and configure your mail to allow sending emails
