<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'auth'], __DIR__ . '/auth.php');

Route::group(['middleware' => 'api'], function () {
    Route::group(['prefix' => 'contacts'], __DIR__ . '/contacts.php');
    Route::group(['prefix' => 'contact-emails'], __DIR__ . '/contactEmails.php');
    Route::group(['prefix' => 'contact-phones'], __DIR__ . '/contactPhones.php');
});

