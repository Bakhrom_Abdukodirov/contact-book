<?php

use App\Http\Controllers\ContactEmailsController;
use Illuminate\Support\Facades\Route;

Route::post('/create', [ContactEmailsController::class, 'createAndUpdate']);
Route::put('/update', [ContactEmailsController::class, 'createAndUpdate']);
Route::delete('/delete/{contact_email}', [ContactEmailsController::class, 'delete']);

