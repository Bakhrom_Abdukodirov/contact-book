<?php

use App\Http\Controllers\ContactPhonesController;
use Illuminate\Support\Facades\Route;

Route::post('/create', [ContactPhonesController::class, 'createAndUpdate']);
Route::put('/update', [ContactPhonesController::class, 'createAndUpdate']);
Route::delete('/delete/{contact_phone}', [ContactPhonesController::class, 'delete']);

