<?php

use App\Http\Controllers\ContactsController;
use Illuminate\Support\Facades\Route;

Route::get('/search', [ContactsController::class, 'search']);
Route::get('/list', [ContactsController::class, 'list']);
Route::post('/create', [ContactsController::class, 'create']);
Route::put('/update/{contact}', [ContactsController::class, 'update']);
Route::delete('/delete/{contact}', [ContactsController::class, 'delete']);

