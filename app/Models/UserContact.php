<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserContact extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'user_contacts';
    protected $fillable = ['user_id', 'contact_id', 'created_at', 'updated_at'];
}
