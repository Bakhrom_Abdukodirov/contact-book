<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class ContactPhone extends \Illuminate\Database\Eloquent\Model
{
    use HasFactory;
    protected $table = 'contact_phones';

    /**
     * @var string[]
     */
    protected $fillable = ['contact_id', 'phone', 'created_at', 'updated_at'];


    /**
     * @param $data
     * @return void
     */
    public static function createAndUpdate($data): void
    {
        foreach ($data['phones'] as $phone) {
            self::query()
                ->updateOrCreate([
                    'contact_id' => $data['contact_id'],
                    'phone' => $phone
                ]);
        }
    }
}
