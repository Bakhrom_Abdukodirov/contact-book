<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class ContactEmail extends \Illuminate\Database\Eloquent\Model
{
    use HasFactory;
    protected $table = 'contact_emails';

    /**
     * @var string[]
     */
    protected $fillable = ['contact_id', 'email', 'created_at', 'updated_at'];

    /**
     * @param $data
     * @return void
     */
    public static function createAndUpdate($data): void
    {
        foreach ($data['emails'] as $email){
            self::query()
                ->updateOrCreate([
                    'contact_id' => $data['contact_id'],
                    'email' => $email
                ]);
        }
    }
}
