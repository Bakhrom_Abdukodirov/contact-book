<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property ContactPhone $phones
 * @property ContactEmail $emails
 */
class Contact extends \Illuminate\Database\Eloquent\Model
{
    use HasFactory;
    protected $table = 'contacts';
    protected $fillable = ['full_name', 'birth_date', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function emails()
    {
        return $this->hasMany(ContactEmail::class, 'contact_id', 'id')->select(['contact_id', 'email']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function phones()
    {
        return $this->hasMany(ContactPhone::class, 'contact_id', 'id')->select(['contact_id', 'phone']);
    }
}
