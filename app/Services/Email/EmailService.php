<?php

namespace App\Services\Email;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class EmailService implements Email
{

    public function send($from, $to, $text, $subject): bool
    {
        $data = ['text' => $text];
        try {
            Mail::send('email.mail', $data, function ($message) use ($from, $to, $subject) {
                $message->to($to)->subject($subject);
                $message->from($from);
            });
            return true;
        } catch (\Exception $e) {
            Log::error('#sendEmail-' . $e->getMessage());
            return false;
        }
    }
}
