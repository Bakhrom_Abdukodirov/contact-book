<?php

namespace App\Services\Email;

interface Email
{
    public function send($from, $to, $text, $subject);
}
