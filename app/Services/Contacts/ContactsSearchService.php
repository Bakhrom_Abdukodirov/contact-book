<?php

namespace App\Services\Contacts;

use App\Models\Contact;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ContactsSearchService
{
    /**
     * @param Request $request
     * @param $user
     * @return object
     */
    public static function search(Request $request, $user): object
    {
        return Contact::query()
            ->leftJoin('contact_emails', 'contact_emails.contact_id', 'contacts.id')
            ->leftJoin('contact_phones', 'contact_phones.contact_id', 'contacts.id')
            ->leftJoin('user_contacts', 'user_contacts.contact_id', 'contacts.id')
            ->leftJoin('users', 'users.id', 'user_contacts.user_id')
            ->where('user_contacts.user_id', $user->id)
            ->when(isset($request->full_name), function (Builder $query) use ($request) {
                $query->where('contacts.full_name', 'like', '%' . $request->full_name . '%');
            })
            ->when(isset($request->email), function (Builder $query) use ($request) {
                $query->where('contact_emails.email', $request->email);
            })
            ->when(isset($request->phone), function (Builder $query) use ($request) {
                $query->where('contact_phones.phone', $request->phone);
            })
            ->select('contacts.id', 'contacts.full_name', 'contacts.birth_date')
            ->with(['emails', 'phones'])
            ->get();
    }
}
