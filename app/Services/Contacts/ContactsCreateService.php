<?php

namespace App\Services\Contacts;

use App\Models\Contact;
use App\Models\UserContact;

class ContactsCreateService
{
    /**
     * @param $data
     * @param $user
     * @return bool
     */
    public static function create($data, $user): bool
    {
        $contact = new Contact();
        $contact->fill($data);
        if($contact->save()){
            if(self::createUserContact($contact, $user))
                return true;
        }

        return false;
    }

    /**
     * @param $contact
     * @param $user
     * @return bool
     */
    public static function createUserContact($contact, $user): bool
    {
        $user_contact = [
            'user_id' => $user->id,
            'contact_id' => $contact->id
        ];
        $user_contacts_create = new UserContact();
        $user_contacts_create->fill($user_contact);
        if($user_contacts_create->save())
            return true;

        return false;
    }
}
