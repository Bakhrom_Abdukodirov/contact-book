<?php

namespace App\Services\Contacts;

use App\Models\Contact;

class ContactsListService
{
    /**
     * @param $user
     * @return object
     */
    public static function list($user): object
    {
        $contacts = Contact::query()
            ->select('contacts.full_name', 'contacts.birth_date')
            ->leftJoin('user_contacts', 'user_contacts.contact_id', 'contacts.id')
            ->leftJoin('users', 'users.id', 'user_contacts.user_id')
            ->where('user_contacts.user_id', $user->id)
            ->get();

        return $contacts;
    }
}
