<?php

namespace App\Console\Commands;

use App\Jobs\SendEmail;
use App\Models\Contact;
use Illuminate\Console\Command;

class BirthDayNotificationCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification, to user, about birthdays of contacts';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $birthday_today_contacts = Contact::query()
            ->leftJoin('user_contacts', 'user_contacts.contact_id', 'contacts.id')
            ->leftJoin('users', 'users.id', 'user_contacts.user_id')
            ->whereMonth('contacts.birth_date', '=', now()->format('m'))
            ->whereDay('contacts.birth_date', '=', now()->format('d'))
            ->select(['users.id as user_id', 'users.email as user_email',
                'contacts.id as contact_id', 'contacts.full_name', 'contacts.birth_date'])
            ->get()->groupBy('user_email');

        if ($birthday_today_contacts){
            foreach ($birthday_today_contacts as $key => $items){
                $text = 'Today is birthday of: <br>';
                foreach ($items as $item){
                    $text .= $item->full_name . ' (' . $item->birth_date . '). <br>';
                }
                dispatch(new SendEmail($key, 'Birth days', $text));
            }
        }
    }
}
