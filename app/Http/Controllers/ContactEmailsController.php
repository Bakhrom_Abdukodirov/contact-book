<?php

namespace App\Http\Controllers;


use App\Http\Requests\ContactEmailCreateAndUpdateRequest;
use App\Models\ContactEmail;
use Illuminate\Http\JsonResponse;

class ContactEmailsController extends Controller
{
    /**
     * @param ContactEmailCreateAndUpdateRequest $request
     * @return JsonResponse
     */
    public function createAndUpdate(ContactEmailCreateAndUpdateRequest $request): JsonResponse
    {
        $user = \auth('api')->user();
        if ($user) {
            $data = $request->validated();
            ContactEmail::createAndUpdate($data);
            return $this->success(['message' => __('Successfully added')]);
        }
        return $this->error(['code'=>401, 'message'=>'Unauthorized'], __('ContactEmail add Error'));
    }

    /**
     * @param ContactEmail $contact_email
     * @return JsonResponse
     */
    public function delete(ContactEmail $contact_email): JsonResponse
    {
        $user = \auth('api')->user();
        if ($user) {
            $contact_email->delete();
            return $this->success(['message' => __('Successfully deleted')]);
        }
        return $this->error(['code'=>401, 'message'=>'Unauthorized'], __('ContactEmail delete Error'));
    }
}
