<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactPhoneCreateAndUpdateRequest;
use App\Models\ContactEmail;
use App\Models\ContactPhone;
use Illuminate\Http\JsonResponse;

class ContactPhonesController extends Controller
{
    /**
     * @param ContactPhoneCreateAndUpdateRequest $request
     * @return JsonResponse
     */
    public function createAndUpdate(ContactPhoneCreateAndUpdateRequest $request): JsonResponse
    {
        $user = \auth('api')->user();
        if ($user) {
            $data = $request->validated();
            ContactPhone::createAndUpdate($data);
            return $this->success(['message' => __('Successfully added')]);
        }
        return $this->error(['code' => 401, 'message' => 'Unauthorized'], __('ContactPhone add Error'));
    }

    /**
     * @param ContactPhone $contact_phone
     * @return JsonResponse
     */
    public function delete(ContactPhone $contact_phone): JsonResponse
    {
        $user = \auth('api')->user();
        if ($user) {
            $contact_phone->delete();
            return $this->success(['message' => __('Successfully deleted')]);
        }
        return $this->error(['code' => 401, 'message' => 'Unauthorized'], __('ContactPhone delete Error'));
    }
}
