<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactCreateRequest;
use App\Http\Requests\ContactSearchRequest;
use App\Models\Contact;
use App\Services\Contacts\ContactsCreateService;
use App\Services\Contacts\ContactsListService;
use App\Services\Contacts\ContactsSearchService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ContactsController extends Controller
{
    /**
     * @param ContactSearchRequest $request
     * @return JsonResponse
     */
    public function search(ContactSearchRequest $request): JsonResponse
    {
        $user = \auth('api')->user();
        if ($user) {
            $request->validated();
            $contacts = ContactsSearchService::search($request, $user);
            return $this->success($contacts);
        }

        return $this->error(['code' => 401, 'message' => 'Unauthorized'], __('Contact search Error'));
    }

    /**
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        $user = \auth('api')->user();
        if ($user) {
            $contacts = ContactsListService::list($user);
            return $this->success($contacts);
        }
        return $this->error(['code' => 401, 'message' => 'Unauthorized'], __('Contact list get Error'));
    }

    /**
     * @param ContactCreateRequest $request
     * @return JsonResponse
     */
    public function create(ContactCreateRequest $request): JsonResponse
    {
        $user = \auth('api')->user();
        if ($user) {
            $data = $request->validated();
            DB::beginTransaction();
            try {
                if (ContactsCreateService::create($data, $user)) {
                    DB::commit();
                    return $this->success(['message' => __('Successfully created')]);
                }
                return $this->error(['message' => [__('Contact create Error')]]);
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error('CreateContactContactsController#error:' . $e->getMessage());
                return $this->error(['message' => [__('Contact create Error')]]);
            }
        }
        return $this->error(['code' => 401, 'message' => 'Unauthorized'], __('Contact create Error'));
    }

    /**
     * @param Request $request
     * @param Contact $contact
     * @return JsonResponse
     */
    public function update(Request $request, Contact $contact): JsonResponse
    {
        $user = \auth('api')->user();
        if ($user) {
            $data = $request->validate([
                'full_name' => 'required|string|max:255|unique:contacts,full_name,' . $contact->id,
                'birth_date' => 'required|date',
            ]);
            if ($contact->update($data))
                return $this->success(['message' => __('Successfully updated')]);

            return $this->error(['message' => __('Update error')]);
        }
        return $this->error(['code' => 401, 'message' => 'Unauthorized'], __('Contact update Error'));
    }

    /**
     * @param Contact $contact
     * @return JsonResponse
     */
    public function delete(Contact $contact): JsonResponse
    {
        $user = \auth('api')->user();
        if ($user) {
            $contact->delete();
            return $this->success(['message' => __('Successfully deleted')]);
        }
        return $this->error(['code' => 401, 'message' => 'Unauthorized'], __('Contact delete Error'));
    }
}
