<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request):  JsonResponse
    {
        $data = $request->validate([
            'email' => 'required|string|max:255',
            'password' => 'required|string|max:255',
        ]);

        $user = User::query()->where('email', $data['email'])->first();
        if (!$user || !Hash::check($data['password'], $user->password))
        return $this->error(['email' => [__('Incorrect email or password')]]);

        if ($user->api_token) {
            $check_user = JWTAuth::setToken($user->api_token);
            if ($check_user->check() == true)
                JWTAuth::invalidate();
        }
        $token = auth('api')->login($user);

        return $this->respondWithToken($token);
    }

    /**
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        \auth('api')->logout();
        return $this->success(['message' => __('Successfully logged out')]);
    }

    /**
     * @param $token
     * @return JsonResponse
     */
    protected function respondWithToken($token): JsonResponse
    {
        return $this->success(['token' => $token]);
    }
}
